#ifndef __SQLITE3_EMBEDDED_H
#define __SQLITE3_EMBEDDED_H

#include "sqlite3.h"

#ifdef __cplusplus
extern "C" {
#endif
    int sqlite3_cmd_main(void);

#define SQLITE_SHELL_DBNAME_PROC getDatabaseName


#ifdef __cplusplus
}
#endif

#endif