#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/deps/sqlite/shell.o \
	${OBJECTDIR}/deps/sqlite/sqlite3.o \
	${OBJECTDIR}/src/original/program.o \
	${OBJECTDIR}/src/sqlite/SQLiteConnection.o \
	${OBJECTDIR}/src/sqlite/SQLiteQuery.o \
	${OBJECTDIR}/src/sqlite/SQLiteQueryAssignment.o \
	${OBJECTDIR}/src/sqlite/main.o \
	${OBJECTDIR}/src/sqlite/program.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/program.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/program.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/program ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/deps/sqlite/shell.o: deps/sqlite/shell.c 
	${MKDIR} -p ${OBJECTDIR}/deps/sqlite
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/deps/sqlite/shell.o deps/sqlite/shell.c

${OBJECTDIR}/deps/sqlite/sqlite3.o: deps/sqlite/sqlite3.c 
	${MKDIR} -p ${OBJECTDIR}/deps/sqlite
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/deps/sqlite/sqlite3.o deps/sqlite/sqlite3.c

${OBJECTDIR}/src/original/program.o: src/original/program.cc 
	${MKDIR} -p ${OBJECTDIR}/src/original
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/original/program.o src/original/program.cc

${OBJECTDIR}/src/sqlite/SQLiteConnection.o: src/sqlite/SQLiteConnection.cc 
	${MKDIR} -p ${OBJECTDIR}/src/sqlite
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sqlite/SQLiteConnection.o src/sqlite/SQLiteConnection.cc

${OBJECTDIR}/src/sqlite/SQLiteQuery.o: src/sqlite/SQLiteQuery.cc 
	${MKDIR} -p ${OBJECTDIR}/src/sqlite
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sqlite/SQLiteQuery.o src/sqlite/SQLiteQuery.cc

${OBJECTDIR}/src/sqlite/SQLiteQueryAssignment.o: src/sqlite/SQLiteQueryAssignment.cc 
	${MKDIR} -p ${OBJECTDIR}/src/sqlite
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sqlite/SQLiteQueryAssignment.o src/sqlite/SQLiteQueryAssignment.cc

${OBJECTDIR}/src/sqlite/main.o: src/sqlite/main.cc 
	${MKDIR} -p ${OBJECTDIR}/src/sqlite
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sqlite/main.o src/sqlite/main.cc

${OBJECTDIR}/src/sqlite/program.o: src/sqlite/program.cc 
	${MKDIR} -p ${OBJECTDIR}/src/sqlite
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/sqlite/program.o src/sqlite/program.cc

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/program.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
