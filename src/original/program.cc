
#include "student.h"
#include "assignment.h"
#include <iostream>
#include <string>
#include <fstream>
#include <Windows.h>
using namespace std;

int Student::sIdCounter = 9000;
int Assignment::aIdCounter = 20150000;
bool backdoor = false;
bool saved = false;

int numStudents = 10;
int numAssignments = 10;

int studentsCapacity = 100;
int assignmentsCapacity = 100;

//creates and intitializes the array
Student roster[100] =  
{
	{"Burrows", "Wade"},
	{"Rodriguez", "Steven"},
	{"Liverman", "Paul"},
	{"Gillespie", "Patrick"},
	{"Swarzenegger", "Arnold"},
	{"Obama", "Barack"},
	{"Bush", "George"},
	{"Fallon", "Jimmy"},
	{"Proudmore", "Jaina"},
	{"Fey", "Tina"}

};

Assignment list[100] =
{
	{"Test 1", "Test", 100, 0},
	{"Test 1", "Test", 100, 79},
	{"Test 1", "Test", 100, 80},
	{"Test 2", "Test", 50, 40},
	{"Test 2", "Test", 50, 41},
	{"Test 2", "Test", 50, 42},
	{"Quiz 1", "Quiz", 30, 27},
	{"Quiz 1", "Quiz", 30, 28},
	{"Quiz 2", "Quiz", 30, 30},
	{"Quiz 2", "Quiz", 30, 32}

};
void queryLN();
void queryPR();
void choice();
void storeStudentsData();
void storeAssignmentsData();
void storeData();
void retrieveData();
int searchLN(Student object[], int size, string value);
int searchPR(Assignment object[], int size, int value);

void backdoorOpen()
{
	cout << "\nThis is where the Backdoor Code would go if we had implemented it yet." << endl;
	cout << "\nPretend you now have full access to all records and can do anything you want" << endl << "with any of the records in the system." << endl;
	cout << "\nWe will now return you to the normal program." << endl << endl;
	cout << "5" << endl;
	Sleep(1200);
	cout << "4" << endl;
	Sleep(1200);
	cout << "3" << endl;
	Sleep(1200);
	cout << "2" << endl;
	Sleep(1200);
	cout << "1" << endl << endl;
	Sleep(1200);
	choice();
}

//exits program unceremoniously
int exit()
{
	return 0;
}
//this gives user the choice of searching again
void again()
{
	char yn;

	cout << "\nWould you like to search again? (Y/N) ";
	cin >> yn;

	switch (yn)
	{
	case 'Y': //fallthrough intentional
	case 'y':
		cout << endl;
		choice();
		break;
	case 'N': //fallthrough intentional
	case 'n':
		if (saved == false){
			cout << "Data is unsaved. Would you like to store data? (Y/N) ";
			cin >> yn;
			if (yn == 'y'){
				storeStudentsData();
				storeAssignmentsData();
			}
		}
		cout << "\nGoodbye.";
		Sleep(1000);
		exit();
		break;
	default:
		again();
		break;
	}
}

void addStudent(){
	string firstName;
	string lastName;
	cout << "Enter First Name: ";
	cin >> firstName;
	cout << "Enter Last Name: ";
	cin >> lastName;
	int pos = searchLN(roster, numStudents, lastName);
	if (pos == -1){
		if (numStudents == studentsCapacity){
			cout << "Student list is full" << endl;
			exit();
		}
		else{
			roster[numStudents++] = Student(lastName, firstName);
		}
	}
	else{
		cout << "Student already exists" << endl;

		//The object was found
		cout << "StudentID: " << roster[pos].get_id() << endl;
		cout << "First Name: " << roster[pos].get_firstname() << endl;
		cout << "Last Name: " << roster[pos].get_lastname() << endl;
	}
	again();
}

void addAssignment(){
	string name, type;
	int m_poss, m_recv;
	cout << "Enter Assignment Name: ";
	cin >> name;
	cout << "Enter Assignment Type: ";
	cin >> type;
	cout << "Enter Marks possessed: ";
	cin >> m_poss;
	cout << "Enter Marks Received: ";
	cin >> m_recv;
	int pos = searchPR(list, numAssignments, m_recv);
	if (pos == -1){
		if (numAssignments == assignmentsCapacity){
			cout << "Assignments list is full" << endl;
			exit();
		}
		else{
			list[numAssignments++] = Assignment(name, type, m_poss, m_recv);
		}
	}
	else{
		cout << "Assignment already exists" << endl;

		//The object was found
		cout << "AssignmentID: " << list[pos].get_id() << endl;
		cout << "Assignment Name: " << list[pos].get_name() << endl;
		cout << "Assignment Type: " << list[pos].get_type() << endl;
		cout << "Points Possible: " << list[pos].get_poss() << endl;
		cout << "Points Received: " << list[pos].get_recd() << endl;
		cout << "Grade: " << list[pos].get_grade() * 100 << "%" << endl;
	}
	again();
}

void storeStudentsData(){
	ofstream out;
	out.open("studentsData.txt");
	for (int i = 0; i < numStudents; ++i){
		out << roster[i].get_id() << endl;
		out << roster[i].get_firstname() << endl;
		out << roster[i].get_lastname() << endl << endl;
	}
}

void storeAssignmentsData(){
	ofstream out;
	out.open("AssignmentsData.txt");
	for (int i = 0; i < numAssignments; ++i){
		out << list[i].get_id() << endl;
		out << list[i].get_name() << endl;
		out << list[i].get_type() << endl;
		out << list[i].get_poss() << endl;
		out << list[i].get_recd() << endl;
		out << list[i].get_grade() << endl << endl << endl;
	}
}

void storeData(){
	storeStudentsData();
	storeAssignmentsData();
	again();
}

void retrieveStudentsData(){
	ifstream in;
	in.open("StudentsData.txt");
	int id;
	string firstName, lastName;
	numStudents = 0;
	if (in.is_open()){
		while (in >> id){
			in >> firstName;
			in >> lastName;
			if (numStudents == studentsCapacity)
				break;
			roster[numStudents++] = Student(lastName, firstName);
		}
	}
	else{
		cout << "Can not open StudentsData.txt" << endl;
	}
}

void retrieveAssignmentsData(){
	ifstream in;
	in.open("AssignmentsData.txt");
	int id;
	string name, type;
	int poss, recv;
	double grade;
	numAssignments = 0;
	if (in.is_open()){
		while (in >> id){
			getline(in, name);
			getline(in, name);
			in >> type;
			in >> poss;
			in >> recv;
			in >> grade;
			cout << grade << endl;
			if (numAssignments == assignmentsCapacity)
				break;
			list[numAssignments++] = Assignment(name, type, poss, recv);
		}
	}
	else{
		cout << "Can not open AssignmentsData.txt" << endl;
	}
}

void retrieveData(){
	retrieveStudentsData();
	retrieveAssignmentsData();
	again();
}

//initial choice to begin the program
void choice()
{
	char selection;


	cout << "What type of search would you like to perform ?" << endl << endl;
	cout << "1. Search by Points Received" << endl;
	cout << "2. Search by Last Name" << endl;
	cout << "3. Add a student" << endl;
	cout << "4. Add an assignment" << endl;
	cout << "5. Store data into file" << endl;
	cout << "6. Retrieve Information from file" << endl << endl << endl << endl;
	cout << "Enter your choice: ";
	cin >> selection;

	switch (selection)
	{
	case '1':
		backdoor = false;
		queryPR();
		break;
	case '2':
		backdoor = false;
		queryLN();
		break;
	case '3':
		backdoor = false;
		saved = false;
		addStudent();
		break;
	case '4':
		backdoor = false;
		saved = false;
		addAssignment();
		break;
	case '5':
		backdoor = false;
		storeData();
		saved = true;
		break;
	case '6':
		backdoor = false;
		retrieveData();
		break;
	case '!':

		if (backdoor == true)
		{

			backdoorOpen();
			break;
		}
		else
		{

			backdoor = true;
			cout << "\nPlease make a valid selection." << endl << endl;
			choice();
			break;
		}
	default:
		backdoor = false;

		cout << "\nPlease make a valid selection." << endl << endl;
		choice();
		break;
	}
}



//searches array by Last Name
int searchLN(Student object[], int size, string value)
{
	int index = 0; //used as a subscript to search array
	int position = -1; //used to record position of search value
	bool found = false; //flag to indicate if the value was found

	while (index < size && !found)
	{
		if (object[index].get_lastname() == value) //if the value is found
		{
			found = true; //sets found flag to true
			position = index; //record the value's subscript
		}
		index++; //go to next element
	}
	return position; //returns the position of the element, or -1
} //end search
//searches array by Points Received
int searchPR(Assignment object[], int size, int value)
{
	int index = 0; //used as a subscript to search array
	int position = -1; //used to record position of search value
	bool found = false; //flag to indicate if the value was found

	while (index < size && !found)
	{
		if (object[index].get_recd() == value) //if the value is found
		{
			found = true; //sets found flag to true
			position = index; //record the value's subscript
		}
		index++; //go to next element
	}
	return position; //returns the position of the element, or -1
} //end search
void queryLN()
{
	string desiredLN; //the Last Name to look for
	int pos; //position of desired object in array

	//get Last Name to search for
	cout << "\nEnter a Last Name (case sensitive): ";
	cin >> desiredLN;

	//search for the object
	pos = searchLN(roster, numStudents, desiredLN);

	//if pos = -1, the code was not found
	if (pos == -1)
		cout << "\nThat Last Name does not exist." << endl;
	else
	{
		//The object was found
		cout << "StudentID: " << roster[pos].get_id() << endl;
		cout << "First Name: " << roster[pos].get_firstname() << endl;
		cout << "Last Name: " << roster[pos].get_lastname() << endl;

	}
	again();
}
void queryPR()
{
	int desiredPR; //the Point Received to look for
	int pos; //position of desired object in array

	//get Last Name to search for
	cout << "\nEnter Point Received: ";
	cin >> desiredPR;

	//search for the object
	pos = searchPR(list, numAssignments, desiredPR);

	//if pos = -1, the code was not found
	if (pos == -1)
		cout << "\nThat amount of points does not exist." << endl;
	else
	{
		//The object was found
		cout << "AssignmentID: " << list[pos].get_id() << endl;
		cout << "Assignment Name: " << list[pos].get_name() << endl;
		cout << "Assignment Type: " << list[pos].get_type() << endl;
		cout << "Points Possible: " << list[pos].get_poss() << endl;
		cout << "Points Received: " << list[pos].get_recd() << endl;
		cout << "Grade: " << list[pos].get_grade() * 100 << "%" << endl;

	}
	again();
}
//This keeps the program open at the end so we can see the results.
void endProgram(){
	//commented out code should be commented back if cin is used in any of the functions
	/**/ cin.clear();
	cin.ignore(255, '\n');/**/
	cin.get();
}

int main(){

	choice();
	endProgram();
}