#ifndef STUDENT_H
#define STUDENT_H

#include <iostream>
#include <string>
using namespace std;

//class for students
class Student{

private:
	static int sIdCounter;
	int m_nStudentId;
	string m_strLastName;
	string m_strFirstName;

public:

	//default constructor
	Student()
	{
		m_nStudentId = -1;
		m_strLastName = "VOID";
		m_strFirstName = "VOID";
	}

	// copy constructor

	Student(Student &s){
		m_nStudentId = s.m_nStudentId;
		m_strFirstName = s.m_strFirstName;
		m_strLastName = s.m_strLastName;
	}

	//constructor with parameters
	Student(string init_lastname, string init_firstname)
	{
		sIdCounter++;
		m_nStudentId = sIdCounter;
		m_strLastName = init_lastname;
		m_strFirstName = init_firstname;
	}

	//gets the class member information
	int get_id(){ return m_nStudentId; }
	string get_lastname(){ return m_strLastName; }
	string get_firstname(){ return m_strFirstName; }

	//sets the class member information
	void set_lastname(string init_lastname){ m_strLastName = init_lastname; }
	void set_firstname(string init_firstname){ m_strFirstName = init_firstname; }
};

#endif