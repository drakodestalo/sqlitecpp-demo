#include <iostream>
#include <string>

#include <sqlite3.h>

#include "SQLiteConnection.h"

namespace sqlite {
    
    SQLiteConnection::~SQLiteConnection() {
        sqlite3_close(db);
    }
    
    SQLiteConnection::SQLiteConnection(std::string filename) {
		int rVal;
        rVal = sqlite3_open(filename.c_str(),&db);
		if (rVal != SQLITE_OK) {
			throw rVal;
		}
		
    }
    
    sqlite3 *& SQLiteConnection::getConnection( void ) {
        return db;
    }
    
    
    
}