#ifndef __SQLITE_CONNECTION_H
#define __SQLITE_CONNECTION_H

#include <string>
#include <sqlite3.h>

namespace sqlite {
    
    
    class SQLiteConnection {
        private:
            sqlite3 *db;
        
        public:
            // Opens database connection
            SQLiteConnection(std::string filename);
            
            // Closes database connection
            ~SQLiteConnection();
            
            
            sqlite3 *& getConnection( void );
    };

}

#endif