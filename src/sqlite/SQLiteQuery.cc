#include <string>
#include "SQLiteQuery.h"


namespace sqlite {
    
    void SQLiteQuery::executeQuery(std::string iSQL) {
        int rVal;
        
        rVal = sqlite3_prepare_v2(this->dbConn,iSQL.c_str(),iSQL.size(),&this->stmt,NULL);
        if (rVal != SQLITE_OK) {
            throw rVal;
        }
        
    }
	void SQLiteQuery::bindValue(int nPos, std::string value) {
	    sqlite3_bind_text(this->stmt,nPos,value.c_str(),value.size(), SQLITE_STATIC );
	}
	
	void SQLiteQuery::bindValue(int nPos, int value) {
		sqlite3_bind_int(this->stmt,nPos, value);
	}
    
    int SQLiteQuery::step( void ) {
        int rVal;
        
        rVal = sqlite3_step(this->stmt);
        
        return rVal;
    }
    
    void SQLiteQuery::closeConnection( void ) {
        if (this->stmt != NULL) {
            sqlite3_finalize(this->stmt);
            this->stmt = NULL;
        }
    }
    
    SQLiteQuery::~SQLiteQuery() {
        closeConnection();
    }
}