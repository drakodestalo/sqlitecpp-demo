#ifndef __SQLITEQUERY_H
#define __SQLITEQUERY_H

#include <string>
#include <iostream>

#include <sqlite3.h>

#include "SQLiteConnection.h"

namespace sqlite {
    
    class SQLiteQuery {
        protected:
            sqlite3 *dbConn;
            sqlite3_stmt *stmt;
            
        public:
            SQLiteQuery(sqlite3*& sConn) : dbConn(sConn), stmt(NULL) {}
            ~SQLiteQuery();
            
            void executeQuery(std::string);
			
			void bindValue(int nPos, int value);
			void bindValue(int nPos, std::string value);
            
            int step( void );
            void printDebug( void );
            
            void closeConnection( void );
    };
    
}

#endif