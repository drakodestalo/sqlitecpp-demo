#include <string>
#include <iostream>
#include <sqlite3.h>
#include "SQLiteQueryAssignment.h"

namespace sqlite {
	
	
    
    int SQLiteQueryAssignment::step ( void ) {
        
        int rVal;
		
		try {
			rVal = SQLiteQuery::step();
			
			if (rVal == SQLITE_ROW) {
				// Start collecting info from the database
				this->assignmentId = sqlite3_column_int( this->stmt, 0);
				this->assignmentName= std::string((const char *)sqlite3_column_text( this->stmt, 1));
				this->assignmentType = std::string((const char *)sqlite3_column_text( this->stmt, 2));
				this->pointsPoss = sqlite3_column_int ( this->stmt, 3);
				this->pointsRecv = sqlite3_column_int (this->stmt, 4);
				this->pct_grade = sqlite3_column_double (this->stmt, 5);
			}
		} catch ( int e) {
			std::cerr << "SQLite error: " << e;
		}
		return rVal;
    }
    
    unsigned long SQLiteQueryAssignment::get_id ( void ) { return this->assignmentId; }
    std::string SQLiteQueryAssignment::get_name( void ) { return this->assignmentName; }
	std::string SQLiteQueryAssignment::get_type ( void ) { return this->assignmentType; }
	int SQLiteQueryAssignment::get_poss( void ) { return this->pointsPoss; }
	int SQLiteQueryAssignment::get_recd( void ) { return this->pointsRecv; }
	double SQLiteQueryAssignment::get_grade ( void ) { return this->pct_grade; }
	
	
}