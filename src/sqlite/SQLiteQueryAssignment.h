#include <string>
#include "SQLiteQuery.h"

namespace sqlite {
    
    class SQLiteQueryAssignment : public SQLiteQuery {
        private:
            unsigned long assignmentId;
            std::string assignmentName;
            std::string assignmentType;
			int pointsPoss;
			int pointsRecv;
			double pct_grade;
        public:
			SQLiteQueryAssignment(sqlite3* sConn) : SQLiteQuery::SQLiteQuery(sConn) { }
            int step( void );
            unsigned long get_id( void );
			std::string get_name( void );
			std::string get_type ( void );
			int get_poss( void );
			int get_recd ( void );
			double get_grade ( void );
			
    };
    
}