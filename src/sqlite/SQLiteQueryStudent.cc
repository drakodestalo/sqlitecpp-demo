#include "SQLiteQueryStudent.h"

namespace sqlite {
    int SQLiteQueryStudent::step ( void ) {
        int rVal;
        try {
            rVal = SQLiteQuery::step();
            
            if (rVal == SQLITE_ROW) {
                studentId = sqlite3_column_int( this->stmt, 0);
                studentFirstName = std::string((const char *) sqlite3_column_text (this->stmt,1));
                studentLastName =  std::string((const char *)sqlite3_column_text( this->stmt, 2));
            }
        } catch ( int e) {
            std::cerr << "SQLite error: " << e;
        }
        return rVal;
    }
    
    int SQLiteQueryStudent::get_id( void ) { return this->studentId;}
    std::string SQLiteQueryStudent::get_firstname( void ) { return this->studentFirstName; }
    std::string SQLiteQueryStudent::get_lastname( void ) { return this->studentLastName; }
}