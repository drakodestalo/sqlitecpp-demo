#ifndef __SQLITEQUERYSTUDENT_H
#define __SQLITEQUERYSTUDENT_H

#include <string>
#include "SQLiteQuery.h"

namespace sqlite {
    class SQLiteQueryStudent : public SQLiteQuery {
        private:
            unsigned long studentId;
            std::string studentFirstName;
            std::string studentLastName;
        public:
            SQLiteQueryStudent(sqlite3 *sConn) : SQLiteQuery::SQLiteQuery(sConn) { }
            int step ( void );
            int get_id (void );
            std::string get_firstname( void );
            std::string get_lastname( void );
            
    };
}

#endif