#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include <iostream>
#include <string>

namespace students {


using namespace std;

//class for assignments
class Assignment{

private:
	int aIdCounter;
	int m_nAssignmentId;
	string m_strAssignmentName;
	string m_strAssignmentType;
	int m_nPointsPoss;
	int m_nPointsRecd;
	double m_dPercentageGrade;
	friend class Backdoor;

public:

	//default constructor
	Assignment()
	{
		m_nAssignmentId = -1;
		m_strAssignmentName = "VOID";
		m_strAssignmentType = "VOID";
		m_nPointsPoss = -1;
		m_nPointsRecd = -1;
		m_dPercentageGrade = -1.00;
	}

	// copy constructor
	Assignment(Assignment &A){
		m_nAssignmentId = A.m_nAssignmentId;
		m_strAssignmentName = A.m_strAssignmentName;
		m_strAssignmentType = A.m_strAssignmentType;
		m_nPointsPoss = A.m_nPointsPoss;
		m_nPointsRecd = A.m_nPointsRecd;
		m_dPercentageGrade = A.m_dPercentageGrade;
	}

	//constructor with parameters
	Assignment(string init_name, string init_type, int init_poss, int init_recd)
	{
		aIdCounter++;
		m_nAssignmentId = aIdCounter;
		m_strAssignmentName = init_name;
		m_strAssignmentType = init_type;
		m_nPointsPoss = init_poss;
		m_nPointsRecd = init_recd;
		m_dPercentageGrade = (double)m_nPointsRecd / (double)m_nPointsPoss;
	}

	//gets the class member information
	int get_id(){ return m_nAssignmentId; }
	string get_name(){ return m_strAssignmentName; }
	string get_type(){ return m_strAssignmentType; }
	int get_poss(){ return m_nPointsPoss; }
	int get_recd(){ return m_nPointsRecd; }
	double get_grade(){ return m_dPercentageGrade; }

	//sets the class member information
	void set_name(string init_name){ m_strAssignmentName = init_name; }
	void set_type(string init_type){ m_strAssignmentType = init_type; }
	void set_poss(int init_poss){ m_nPointsPoss = init_poss; }
	void set_recd(int init_recd){ m_nPointsRecd = init_recd; }

};

}

#endif