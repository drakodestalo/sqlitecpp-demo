BEGIN TRANSACTION;

insert into students (student_firstname, student_lastname) values ("Burrows", "Wade");
insert into students (student_firstname, student_lastname) values ("Rodriguez", "Steven");
insert into students (student_firstname, student_lastname) values ("Liverman", "Paul");
insert into students (student_firstname, student_lastname) values ("Gillespie", "Patrick");
insert into students (student_firstname, student_lastname) values ("Swarzenegger", "Arnold");
insert into students (student_firstname, student_lastname) values ("Obama", "Barack");
insert into students (student_firstname, student_lastname) values ("Bush", "George");
insert into students (student_firstname, student_lastname) values ("Fallon", "Jimmy");
insert into students (student_firstname, student_lastname) values ("Proudmore", "Jaina");
insert into students (student_firstname, student_lastname) values ("Fey", "Tina");

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Test 1", "Test", 100, 0);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Test 1", "Test", 100, 79);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Test 1", "Test", 100, 80);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Test 2", "Test", 50, 40);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Test 2", "Test", 50, 41);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Test 2", "Test", 50, 42);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Quiz 1", "Quiz", 30, 27);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Quiz 1", "Quiz", 30, 28);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Quiz 2", "Quiz", 30, 30);

insert into assignments (assignment_name, assignment_type, points_possible, points_received)
values ("Quiz 2", "Quiz", 30, 32);

COMMIT;
