CREATE TABLE students (
    student_id          INTEGER PRIMARY KEY,
    student_firstname   TEXT,
    student_lastname    TEXT
);

CREATE TABLE assignments (
    assignment_id       INTEGER PRIMARY KEY,
    assignment_name     TEXT,
    assignment_type     TEXT,
    points_possible     INTEGER,
    points_received     INTEGER,
    pct_grade           REAL
);