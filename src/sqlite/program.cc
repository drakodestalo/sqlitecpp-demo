#include <iostream>
#include <string>
#include <fstream>

#ifdef _win32_
#include <Windows.h>
#endif

#ifdef _linux_
#include <unistd.h>
#endif


#include "student.h"
#include "assignment.h"
#include "program.h"
#include "sqlite3_embedded.h"

#include "SQLiteConnection.h"

#include "SQLiteQueryAssignment.h"
#include "SQLiteQueryStudent.h"

extern "C" {
	void getDatabaseName(char ** par) {
		static char databaseName[] ="program.db";
		*par = databaseName;
	}
}

namespace program {
	
	using namespace std;
	
	/* sleep_wrap
	 *
	 * This function wraps the sleep function across different OS and platforms
	  */
	void Program::sleep_wrap( int nsec ) {
			#ifdef _linux_
			sleep(nsec);
			#endif
			
			#ifdef _win32_
			Sleep(nsec);
			#endif
	}
		
	/* Program constructor
	   immediate initializes database connection
	*/
	Program::Program() : dbConn(DATABASE_NAME) {
		
			this->backdoor = false;
			this->saved = false;
			
		}
			
		void Program::backdoorOpen()
		{
			sqlite3_cmd_main();
		}
		
		//exits program unceremoniously
		int Program::exit()
		{
			return 0;
		}
		
		void Program::addStudent(){
			sqlite::SQLiteQuery sqlCmd(dbConn.getConnection());
			sqlite::SQLiteQueryStudent student(dbConn.getConnection());
			
			using namespace std;
			
			string firstName;
			string lastName;
			cout << "Enter First Name: ";
			cin >> firstName;
			cout << "Enter Last Name: ";
			cin >> lastName;
			
			student.executeQuery("select * from students where lower(student_lastname) = lower(:1) and lower(student_firstname) = lower(:2)");
			student.bindValue(1,lastName);
			student.bindValue(2,firstName);
			
			try {
			
				if (student.step() != SQLITE_ROW) {
					// student does not exists, add new student
					student.closeConnection();
					
				
					sqlCmd.executeQuery("insert into students (student_lastname, student_firstname) values (:1, :2)");
					sqlCmd.bindValue(1,lastName);
					sqlCmd.bindValue(2,firstName);
					if (sqlCmd.step() == SQLITE_DONE) {
						cout << "Student added successfully!" << endl;
					}
					
				} else {
					cout << "Student already exists" << endl << 
							"StudentID: " << student.get_id() << endl << 
							"First Name: " << student.get_firstname() << endl << 
							"Last Name: " << student.get_lastname() << endl;
				}
			} catch (int e) {
				std::cerr << "SQLite error: "  << sqlite3_errstr(e);
			}
		}
		
		void Program::addAssignment() {
			sqlite::SQLiteQueryAssignment assignment(dbConn.getConnection());
			sqlite::SQLiteQuery query(dbConn.getConnection());
			string name, type;
			int m_poss, m_recv, pos;
			cout << "Enter Assignment Name: ";
			cin >> name;
			cout << "Enter Assignment Type: ";
			cin >> type;
			cout << "Enter Marks possessed: ";
			cin >> m_poss;
			cout << "Enter Marks Received: ";
			cin >> m_recv;
			
			assignment.executeQuery("select * from assignments where lower(assignment_name) = lower(:1)");
			assignment.bindValue(1,name);
			
			try {
				if (assignment.step() != SQLITE_ROW) {
					// assignment dont exist, add new
					query.executeQuery("insert into assignments (assignment_name, assignment_type, points_possible,points_received) values (:1,:2,:3,:4)");
					query.bindValue(1,name);
					query.bindValue(2,type);
					query.bindValue(3,m_poss);
					query.bindValue(4,m_recv);
					if (query.step() == SQLITE_DONE) {
						cout << "Assignment added successfully!" << endl;
					}
				} else {
					cout << "Assignment already exists" << endl <<
							"AssignmentID: " << assignment.get_id() << endl <<
							"Assignment Name: " << assignment.get_name() << endl << 
							"Assignment Type: " << assignment.get_type() << endl <<
							"Points Possible: " << assignment.get_poss() << endl <<
							"Points Received: " << assignment.get_recd() << endl << 
							"Grade: " << assignment.get_grade() * 100 << "%" << endl;
				}
			} catch (int e) {
				std::cerr << "SQLite Error: " << sqlite3_errstr(e);
			}
			
		}
		
		//initial choice to begin the program
		void Program::choice()
		{
			char selection, selAgain;
			
			do {
		
		
				cout << "**SQLite Powered**""" << endl << endl << 
						"What type of search would you like to perform ?" << endl << endl << 
						"1. Search by Points Received" << endl <<
						"2. Search by Last Name" << endl << 
						"3. Add a student" << endl << 
						"4. Add an assignment" << endl << 
						"x. Exit" << endl << endl << endl <<
						"Enter your choice: ";
				cin >> selection;
			
				switch (selection) {
				case '1':
					queryPR();
					break;
				case '2':
					queryLN();
					break;
				case '3':
					saved = false;
					addStudent();
					break;
				case '4':
					saved = false;
					addAssignment();
					break;
				case '!':
			
					if (backdoor == true) backdoorOpen();
					else backdoor = true;
						
					break;
				}
				
				if (selection != '!') backdoor = false;
				if (selection != 'x') {
					cout << endl << "Search again (Y/N) ? ";
					cin >> selAgain;
				
					if (selAgain!= 'Y' and selAgain != 'y') {
						selection = 'x';
					}
				}
			
			} while (selection != 'x');
		}
		
		
		void Program::queryLN()
		{
			sqlite::SQLiteQueryStudent student(dbConn.getConnection());
			string desiredLN; //the Last Name to look for
			int pos; //position of desired object in array
		
			//get Last Name to search for
			cout << "\nEnter a Last Name (case sensitive): ";
			cin >> desiredLN;
			try {
				student.executeQuery("select * from students where lower(student_lastname) = lower(:1)");
				student.bindValue(1,desiredLN);
				
				if (student.step() != SQLITE_ROW)
					cout << "\nThat Last Name does not exist." << endl;
				else
				{
					//The object was found
					cout << "StudentID: " << student.get_id() << endl << 
							"First Name: " << student.get_firstname() << endl <<
							"Last Name: " << student.get_lastname() << endl;
			
				}
			} catch ( int e) {
				std::cerr << "SQLite error: " << e;
			}
		}
		void Program::queryPR()
		{
			sqlite::SQLiteQueryAssignment assignment(dbConn.getConnection());
			int desiredPR; //the Point Received to look for
			
			//get Last Name to search for
			cout << "\nEnter Point Received: ";
			cin >> desiredPR;
			
			try {
				assignment.executeQuery("select * from assignments where points_received = :1");
				
				assignment.bindValue(1,desiredPR);
				
				if ( assignment.step() != SQLITE_ROW) {
					cout << "\nThat amount of points does not exist." << endl;
				} else {
					//The object was found
					cout << "AssignmentID: " << assignment.get_id() << endl << 
							"Assignment Name: " << assignment.get_name() << endl <<
							"Assignment Type: " << assignment.get_type() << endl <<
							"Points Possible: " << assignment.get_poss() << endl <<
							"Points Received: " << assignment.get_recd() << endl <<
							"Grade: " << assignment.get_grade() * 100 << "%" << endl;
				}
			} catch (int e) {
				cerr << "SQLite error " << sqlite3_errstr(e);
			}
		}
		//This keeps the program open at the end so we can see the results.
		void Program::endProgram(){
			//commented out code should be commented back if cin is used in any of the functions
			/**/ cin.clear();
			cin.ignore(255, '\n');/**/
			cin.get();
		}
}