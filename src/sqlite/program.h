#ifndef __PROGRAM_H
#define __PROGRAM_H
#define DATABASE_NAME "program.db"

#include "student.h"
#include "assignment.h"

#include "SQLiteConnection.h"

namespace program {
    
    class Program {
        private:
            sqlite::SQLiteConnection dbConn;
            bool backdoor;
            bool saved;
            int numStudents;
            int numAssignments;
            int studentsCapacity;
            int assignmentsCapacity;
            
            students::Student roster[100];
            students::Assignment list[100];
            
            void sleep_wrap (int);
            int searchLN(students::Student object[], int size, std::string value);
            int searchPR(students::Assignment object[], int size, int value);
            void backdoorOpen();
            int exit();
            void again();
            void addStudent();
            void addAssignment();
            void storeStudentsData();
            void storeAssignmentsData();
            void storeData();
            void retrieveStudentsData();
            void queryLN();
            void retrieveAssignmentsData();
            void retrieveData();
            void queryPR();
        public:
            Program();
            void choice();
            void endProgram();
            
    };
}

#endif